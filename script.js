/*
1. Опишіть, як можна створити новий HTML тег на сторінці.
тег на сторінці можна створити за допомогою методу - document.createElement(tag)

 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.

elem.insertAdjacentHTML(position, text);
position - це параметр (спеціальне слово), яке вказує, куди по відношенню до elem (якогось тегу)
зробити вставку.

  "beforebegin": вставити беспосередньо перед elem
  "afterbegin": вставити напочатку elem
  "beforeend": вставити в кінець elem
  "afterend": вставити беспосередньо після elem

3.Як можна видалити елемент зі сторінки?
елемент зі сторінки можна видалити за допомогою методу remove()
 */

let newArr = ["1", "2", "3", "sea", "user", 23];
let ul = document.createElement('ul');

function createList (arr, parentUl = document.body) {
    arr.forEach((elem) => {
    let li = document.createElement ('li');
    li.append(elem);
    ul.append(li);
});
    parentUl.prepend(ul);
}

createList(newArr);